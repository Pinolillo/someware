﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public bool IsColliding;
    public AudioSource balaAudio;
    public AudioClip destruyeVirus;
    public float velocidadDisparo = 0.2f;


    private void Start()
    {

    }
    void Update()
    {
        transform.Translate(velocidadDisparo, 0, 0 * Time.deltaTime);

        //Choca
        if (IsColliding == true)
        {


            this.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            this.gameObject.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);


        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "enemigo")
        {
            IsColliding = true;


            //destroy enemigo al que se dispara
            print("si pega la bala");
            Destroy(other.gameObject);
            balaAudio.Play();





        }
        else
            IsColliding = false;
    }


    //bala se destruye al salir de pantalla
    void OnBecameInvisible()
    {
        enabled = false;
        Destroy(gameObject);

    }
}
