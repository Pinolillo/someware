﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Salto : MonoBehaviour
{
    //RANGO DE LA FUERZA DE SALTO/VUELO
    public int jumpVelocity;
    void Update()
    {
        //Si oprime la barra espaciadora entonces puede"volar".
        if (Input.GetKey(KeyCode.W))
           
        {
            GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpVelocity * Time.deltaTime;
        }        
    }
}