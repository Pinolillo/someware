﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    //El objeto que va disparar
    public Object bala;
    public GameObject jugador;
    public int vidas = 3;

    public AudioSource audioHacker;
    public AudioClip dolor;
    public AudioClip laser;
    public Animator AniHacker;

    //Referencia a los enemigos del nivel para prenderlos 

    void Start()
    {
        AniHacker = GetComponent<Animator>();

    }
    void Update()
    {
        //se mueve solito
        transform.Translate(new Vector3(5f, 0, 0) * Time.deltaTime);

        //MOVIMIENTO libre para el jugador
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(new Vector3(-4f, 0, 0) * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(new Vector3(2f, 0, 0) * Time.deltaTime);
        }

        if (Input.GetButtonUp("Jump"))
        {
            Instantiate(bala, transform.position, Quaternion.identity);
            audioHacker.clip = laser;
            audioHacker.Play();
        }

        //Perder si las vidas llegan a 0 aqui meter la escena
        if (vidas == 0)
        {
            Perder();

            GameObject.FindWithTag("vida1").SetActive(false);
            GameObject.FindWithTag("vida2").SetActive(false);
            GameObject.FindWithTag("vida3").SetActive(false);
        }

        //Manejar las vidas
        if (vidas == 2)
        {
            GameObject.FindWithTag("vida1").SetActive(true);
            GameObject.FindWithTag("vida2").SetActive(true);
            GameObject.FindWithTag("vida3").SetActive(false);

        }
        if (vidas == 1)
        {
            GameObject.FindWithTag("vida1").SetActive(true);
            GameObject.FindWithTag("vida2").SetActive(false);
            GameObject.FindWithTag("vida3").SetActive(false);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        //Quitar vidas al chocar
        if (other.transform.CompareTag("enemigo"))
        {
            //prender enemigo 6 y 7
            vidas = vidas - 1;
            AniHacker.SetTrigger("golpe");
            audioHacker.clip = dolor;
            audioHacker.Play();
        }

        /* Script de rebotar al chocar con las barreras */
        if (other.transform.CompareTag("barrera"))
        {
            Debug.Log("Choque con barrera");
            rebote();

        }


    }

    private void Perder()
    {
        if (SceneManager.GetActiveScene() == SceneManager.GetSceneByName("Escenario2"))
        {
            SceneManager.LoadScene("GameOver");
        }
        else

            SceneManager.LoadScene("GameOver2");

    }

    private void rebote()
    {
        //si la posicion en y es mayor a 0
        if (jugador.transform.position.y > 0)
        {
        }
        //En cambio si es menor a 0
        else
        {
        }

    }


}