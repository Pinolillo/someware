﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class tutorialcounterPantallas : MonoBehaviour
{

 public Sprite[] FondosPantallas;
    private int contador;
    public SpriteRenderer FONDOTUTORIAL;
    public Animator Barra1;
    public Animator Barra2;

    // Start is called before the first frame update
    void Start()
    {
        contador = 0;
        FONDOTUTORIAL = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {

            contador = contador + 1;
            print("pasa a siguiente");

        }

        if (contador == 0)
        {

            FONDOTUTORIAL.sprite = FondosPantallas[0];

        }

        if (contador == 1)
        {

            FONDOTUTORIAL.sprite = FondosPantallas[1];

        }

        if (contador == 2)
        {

            FONDOTUTORIAL.sprite = FondosPantallas[2];

        }

        if (contador == 3)
        {

            FONDOTUTORIAL.sprite = FondosPantallas[3];

        }


        if (contador == 4)
        {
            Barra1.SetTrigger("Bajan");
            Barra2.SetTrigger("Bajan");
            PlayerPrefs.SetInt("Cutscene", 4);


        }

    }
}
