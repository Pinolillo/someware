﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiarEsc : MonoBehaviour
{
    public Animator Barra1;
    public Animator Barra2;
    public AudioSource Botones;
    public AudioClip[] SonidosBotones;
 


    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Tutorial()
    {
        if (PlayerPrefs.GetInt("Cutscene") == 2)
        {
            SceneManager.LoadScene("Menu");
        } else

        SceneManager.LoadScene("Tutorial");
    }

    public void Game()
    {
        Barra1.SetTrigger("Bajan");
        Barra2.SetTrigger("Bajan");

        Botones.clip = SonidosBotones[0];

        Botones.Play();

        PlayerPrefs.SetInt("Cutscene", 4);
    }

    public void Game2()
    {
        Barra1.SetTrigger("Bajan");
        Barra2.SetTrigger("Bajan");

        Botones.clip = SonidosBotones[0];

        Botones.Play();

        PlayerPrefs.SetInt("Cutscene", 3);
    }

    public void Cutscene()
    {

        Barra1.SetTrigger("Bajan");
        Barra2.SetTrigger("Bajan");

        PlayerPrefs.SetInt("Cutscene", 1);

        Botones.clip = SonidosBotones[0];

        Botones.Play();


    }

    public void Cutscene2()
    {

        Barra1.SetTrigger("Bajan");
        Barra2.SetTrigger("Bajan");

        PlayerPrefs.SetInt("Cutscene", 2);

        Botones.clip = SonidosBotones[0];

        Botones.Play();


    }

    public void BarrasTransiciones()
    {
       
        if (PlayerPrefs.GetInt("Cutscene") == 1)
        {
            SceneManager.LoadScene("CUTSCENE");
        }

        if (PlayerPrefs.GetInt("Cutscene") == 2)
        {

            SceneManager.LoadScene("CUTSCENE");
        }

        if (PlayerPrefs.GetInt("Cutscene") == 3)
        {

            SceneManager.LoadScene("Escenario2");
        }

        if (PlayerPrefs.GetInt("Cutscene") == 4)
        {

            SceneManager.LoadScene("Escenario1");
        }

        if (PlayerPrefs.GetInt("Cutscene") == 5)
        {
            SceneManager.LoadScene("Moderno2");

        }

        if (PlayerPrefs.GetInt("Cutscene") == 6)
        {
            SceneManager.LoadScene("Moderno1");
        }



    }


    public void Reintentar()
    {
        SceneManager.LoadScene("Escenario1");
    }

    public void Mainmenu()
    {
        SceneManager.LoadScene("Menu");
    }

public void Moderno1(){

        Barra1.SetTrigger("Bajan");
        Barra2.SetTrigger("Bajan");

        PlayerPrefs.SetInt("Cutscene", 6);

        Botones.clip = SonidosBotones[0];

        Botones.Play();
       
}

public void Moderno2(){

        Barra1.SetTrigger("Bajan");
        Barra2.SetTrigger("Bajan");

        PlayerPrefs.SetInt("Cutscene", 5);
       

        Botones.clip = SonidosBotones[0];

        Botones.Play();
    }


    public void Salir()
    {
        Debug.Log("Quit");
        PlayerPrefs.DeleteAll();

        Botones.clip = SonidosBotones[1];

        Botones.Play();

        StartCoroutine("coroutine", 0f);
    }

    private IEnumerator couroutine()
    {
        yield return new WaitForSeconds(0.265f);
        Application.Quit();

    }

}