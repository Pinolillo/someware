﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerModerno2 : MonoBehaviour
{
    public AudioSource audioPlayer;
    public AudioClip empuja;
    public AudioClip muere;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.D))
        {
            transform.Translate(new Vector3(9f, 0, 0) * Time.deltaTime);
            audioPlayer.clip = empuja;
            audioPlayer.Play();
        }
    }

    void OnCollisionEnter2D(Collision2D col){
     if(col.gameObject.tag == "puas")
     {
            audioPlayer.clip = muere;
            audioPlayer.Play();
            print("ME MORÍ IGUAL");
         SceneManager.LoadScene("Perdistegolpegracia");
     }
 }
}
