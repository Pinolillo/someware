﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class moverEnemigo : MonoBehaviour
{
    public AudioSource audioEnemigo;
    public AudioClip muere;
    public Animator animatorEnemy;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(-1.2f, 0, 0) * Time.deltaTime);
    }

 void OnCollisionEnter2D(Collision2D col){
     if(col.gameObject.tag == "puas")
     {


            StartCoroutine(EsperaAQueMuera());

     }


        IEnumerator EsperaAQueMuera()
        {
            print("ME MORÍ");
            audioEnemigo.clip = muere;
            audioEnemigo.Play();
            animatorEnemy.SetTrigger("Destruye");

            print(PlayerPrefs.GetInt("Cutscene"));


            yield return new WaitForSeconds(1.5f);
            SceneManager.LoadScene("Ganastegolpegracia");


        }
    }

    
}
