﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class marcador1 : MonoBehaviour
{
    public GameObject[] enemigos;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < enemigos.Length; i++)
        {
            enemigos[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D colision)
    {
        if (colision.transform.CompareTag("Player"))
        {
            //Prender enemigos
            Debug.Log("Choque con el player");
            for (int i = 0; i < enemigos.Length; i++)
            {
                enemigos[i].SetActive(true);
            }
        }

    }
}
