﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimientoSpawner : MonoBehaviour
{
    public float velocidadBala = 2;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(new Vector3(-velocidadBala, 0, 0) * Time.deltaTime);

    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            print("HolaMundo");
            Destroy(gameObject);
        }


    }

    //DETECTA CUANDO CHOCA CON UNA BALA DEL JUGADOR Y DESTRUYE LA BALA ENEMIGA
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bala")
        {
            print("QuepedoQuepedo");
            Destroy(gameObject);
        }
    }
}
