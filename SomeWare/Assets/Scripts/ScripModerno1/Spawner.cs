﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Object enemigo1; //?Referencia a la bala del enemigo
    public float frecuenciaInicial = 2.1f; //?frecuencia en la que apareceran las balas

    public float intervalo = 2f;
    public float reduccion = 1f;

    void Start()
    {
        StartCoroutine(sacarBalas());//iniciar la coroutina para sacar a los malos
        StartCoroutine(bajarFrecuncia());//inicar el decremento de frecuencia
    }

    //!Corountina para cambiar los valores de la frecuencia bajar el timepo
    IEnumerator bajarFrecuncia()
    {
        //Al inicio salen cada tres segundos Esperar diez segundos y cambiar
        while (true)
        {
            yield return new WaitForSeconds(intervalo);
            if (frecuenciaInicial > 0.2)//este es el limite no puede ser menor a esto o seria imposible
            {
                frecuenciaInicial -= reduccion;//Baja .8 cada 10 ya que espera 7f mas los segundos inicales
            }
            else
            {
                frecuenciaInicial = frecuenciaInicial + 0f;
            }
        }
    }

    //!Coroutina para sacar balas
    IEnumerator sacarBalas()
    {
        while (true)
        {
            yield return new WaitForSeconds(frecuenciaInicial);
            Spawn();
        }
    }

    //!Spawnear enemigos
    void Spawn()
    {
        float posY = Random.Range(-4f, 4f);
        Instantiate(enemigo1, new Vector3(5f, posY, 0), Quaternion.identity);
    }


}