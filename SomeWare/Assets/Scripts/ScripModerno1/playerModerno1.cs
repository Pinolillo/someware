﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playerModerno1 : MonoBehaviour
{
    public int vidas = 3;
    public Object bala;
    //no poder tocar mas teclas hasta cumplir con los textos
    private float inputTimer = 0;
    public float inputMaxTime = 0.675f;

    public AudioSource audioPlayer;
    public AudioClip disparo;
    public AudioClip dano;
    public AudioSource audioPlayerDano;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (inputTimer >= inputMaxTime)
        {
            if (Input.GetKeyUp(KeyCode.Space))
            {
                Instantiate(bala, transform.position, Quaternion.identity);
                inputTimer = 0.5f;
                audioPlayer.clip = disparo;
                audioPlayer.Play();
            }

            if (Input.GetKey(KeyCode.W))

            {
                GetComponent<Rigidbody2D>().velocity = Vector2.up * 400f * Time.deltaTime;
            }
        }

        inputTimer += Time.deltaTime;

        //BLOQUE PARA IMPRIMIR VIDAS 
        if (vidas == 0)
        {
            Perder();

            GameObject.FindWithTag("vida1").SetActive(false);
            GameObject.FindWithTag("vida2").SetActive(false);
            GameObject.FindWithTag("vida3").SetActive(false);
        }
        if (vidas == 2)
        {
            GameObject.FindWithTag("vida1").SetActive(true);
            GameObject.FindWithTag("vida2").SetActive(true);
            GameObject.FindWithTag("vida3").SetActive(false);

        }
        if (vidas == 1)
        {
            GameObject.FindWithTag("vida1").SetActive(true);
            GameObject.FindWithTag("vida2").SetActive(false);
            GameObject.FindWithTag("vida3").SetActive(false);
        }
    }

    private void Perder()
    {
        SceneManager.LoadScene("PerdisteBossFinal");
    }


    //CUANDO TE PEGA UNA BALA YA SE DETECTA EL COMENTARIO ES PARA QUE DESTRUYA EL ENEMIGO
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "balaenemigo")
        {
            print("MematóALV");
            //Destroy (gameObject);
            vidas = vidas - 1;

            audioPlayerDano.Play();

        }
    }
}
