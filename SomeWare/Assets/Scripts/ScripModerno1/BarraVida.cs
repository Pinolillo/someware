﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class BarraVida : MonoBehaviour
{
    public AudioSource audioJefe;
    public AudioClip golpe;
  


    public Scrollbar HealthBar;
    public float Health = 100;
    public bool IsColliding;

    void Start()
    {

    }
    void Update()
    {


        //Choca
        if (IsColliding == true)
        {
            //Destroy(gameObject);
        }



    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Bala")
        {
            IsColliding = true;

            //destroy enemigo al que se dispara
            print("si pega la bala");
            audioJefe.clip = golpe;
            audioJefe.Play();
            //  Destroy(other.gameObject);
            Health -= 1.4f;
            HealthBar.size = Health / 100f;
            if (Health <= 0)
            {
                PlayerPrefs.SetInt("Cutscene", 2);
                SceneManager.LoadScene("Ganaste");
                Debug.Log("Me muero");
            }

        }
        else
            IsColliding = false;
    }


    //bala se destruye al salir de pantalla


}
