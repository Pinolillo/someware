﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Timer : MonoBehaviour
{

    float currentTime = 0f;

    //aquí se cambia el tiempo
    float startingTime = 25f;
    [SerializeField] Text countdownText;

    void Start()
    {
        currentTime = startingTime;
    }

    void Update()
    {

        currentTime -= 1 * Time.deltaTime;
        countdownText.text = currentTime.ToString();

        if (currentTime <= 0.0f)
        {

            timerEnded();
        }

    }

    void timerEnded()
    {
        SceneManager.LoadScene("GameOver2");
    }


}