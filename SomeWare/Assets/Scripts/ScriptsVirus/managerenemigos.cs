﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class managerenemigos : MonoBehaviour
{

    public GameObject enemigo1;
    public GameObject enemigo2;
    public GameObject enemigo3;
    public GameObject enemigo4;
    public GameObject enemigo5;
    public GameObject enemigo6;
    public GameObject enemigo7;

    // Start is called before the first frame update
    void Start()
    {
        enemigo1.gameObject.SetActive(true);
        enemigo2.gameObject.SetActive(false);
        enemigo3.gameObject.SetActive(false);
        enemigo4.gameObject.SetActive(false);
        enemigo5.gameObject.SetActive(false);
        enemigo6.gameObject.SetActive(false);
        enemigo7.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
