﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnerViruses : MonoBehaviour
{
    public GameObject[] obstaculos;
    float c1x = GameObject.Find("contenedorEnemigo1").transform.position.x;
    float c1y = GameObject.Find("contenedorEnemigo2").transform.position.y;


    // Start is called before the first frame update
    void Start()
    {
        Spawn();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void Spawn()
    {
        Vector3 spawnPosition = new Vector3(c1x, c1y, 0);//establecer la posicion entre estos ejes

        int indexNumber = Random.Range(0, obstaculos.Length);
        Instantiate(obstaculos[indexNumber], spawnPosition, Quaternion.identity);//Inicializar la funcion
    }
}
