﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextoCutscene : MonoBehaviour
{
    public TextAsset textoInicio;
    public TextAsset textoFinal;
    public GameObject cajaTexto; 


    // Start is called before the first frame update
    void Start()
    {
        print(PlayerPrefs.GetInt("Cutscene"));

    }

    // Update is called once per frame
    void Update()
    {



        //if player pref es igual a inicio
        if (PlayerPrefs.GetInt("Cutscene") == 1)
        {

            TextboxController.dialogos = textoInicio;
        }

        if (PlayerPrefs.GetInt("Cutscene") == 2)
        {

            TextboxController.dialogos = textoFinal;

        }


    }

  
}
