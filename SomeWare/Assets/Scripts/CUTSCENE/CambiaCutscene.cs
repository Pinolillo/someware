﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiaCutscene : MonoBehaviour
{

    public Sprite[] CutsceneListInicio;
    public Sprite[] CutsceneListFinal;

    public SpriteRenderer Cutscene;
    public int numCutscene;


    // Start is called before the first frame update
    void Start()
    {
        Cutscene = GetComponent<SpriteRenderer>();
        numCutscene = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            numCutscene = numCutscene + 1;
        }

        //dependiendo de player pref, que cargue un batch de imagenes
        if (PlayerPrefs.GetInt("Cutscene") == 1)
        {

            if (numCutscene == 1)
            {
                Cutscene.sprite = CutsceneListInicio[0];
            }

            if (numCutscene == 4)
            {
                Cutscene.sprite = CutsceneListInicio[1];
            }

            if (numCutscene == 6)
            {
                Cutscene.sprite = CutsceneListInicio[2];
            }

            if (numCutscene == 8)
            {
                Cutscene.sprite = CutsceneListInicio[3];
            }

            if (numCutscene == 10)
            {
                Cutscene.sprite = CutsceneListInicio[4];
            }


            }

        if (PlayerPrefs.GetInt("Cutscene") == 2)
        {

            if (numCutscene == 1)
            {
                Cutscene.sprite = CutsceneListFinal[0];
            }

            if (numCutscene == 3)
            {
                Cutscene.sprite = CutsceneListFinal[1];
            }

        }
    }
}
