﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botones : MonoBehaviour
{

    public GameObject PrefabPopup;


    // Start is called before the first frame update
    public void Cierra()
    {
        Destroy(transform.parent.gameObject);

    }

    // Update is called once per frame
    public void SacaUno()
    {
        float Xmax = Random.Range(-188, 188);
        float Ymax = Random.Range(-131, 131);


        Instantiate(PrefabPopup, GameObject.FindGameObjectWithTag("Canvas").transform);
        PrefabPopup.transform.localPosition = new Vector3(Xmax, Ymax, 0);

    }

    public void SacaTres()
    {
        int counter = 0;

        StartCoroutine(Tresi());


        IEnumerator Tresi()
        {


            for (int i = 0; i < 3; i++)
            {
                float Xmax = Random.Range(-188, 188);
                float Ymax = Random.Range(-131, 131);
                print(Xmax + "y" + Ymax);


                yield return new WaitForSeconds(0);

                Instantiate(PrefabPopup, GameObject.FindGameObjectWithTag("Canvas").transform);
                PrefabPopup.transform.localPosition = new Vector3(Xmax, Ymax, 0);
                counter = counter + 1;
                print("contador: " + counter);

                if (counter == 3)
                {

                    yield break;

                }

            }




        }




    }

}
