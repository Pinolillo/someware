﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraficoPopup : MonoBehaviour
{
    private Image RendererPopUp;
    public Sprite[] spritesPopup;

    // Start is called before the first frame update
    void Start()
    {
        RendererPopUp = GetComponent<Image>();
        //crea randomizer para que escoja un sprite de enemigo
        int indice = Random.Range(0, spritesPopup.Length);

        //del indice que se genere, saca el sprite
        RendererPopUp.sprite = spritesPopup[indice];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
